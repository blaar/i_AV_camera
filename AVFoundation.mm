//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//

#include "blQTKit.h"
#include "blc_image.h"

#import <QTKit/QTKit.h>
#import <CoreVideo/CVPixelBuffer.h> 

@interface capture:NSObject
{
@public
    QTCaptureSession *capture_session;
    blc_array image;
    int initialized;
	void *user_data;
	int (*callback)(blc_array *image, void *user_data);
}
@end


@implementation capture
- (id)init
{
    initialized=0;
    
    return self; 
}


-(void)captureOutput:(QTCaptureOutput *)captureOutput didDropVideoFrameWithSampleBuffer:(QTSampleBuffer *)sampleBuffer fromConnection:(QTCaptureConnection *)connection
{
	(void)captureOutput;
	(void)sampleBuffer;
	(void)connection;
}

-(void)captureOutput:(QTCaptureOutput *)captureOutput didOutputVideoFrame:(CVImageBufferRef)videoFrame withSampleBuffer:(QTSampleBuffer *)sampleBuffer fromConnection:(QTCaptureConnection *)connection
{
    int bytes_per_pixel;
    int continue_run=1;
	(void)captureOutput;
	(void)sampleBuffer;	
	(void)connection;

    if (!initialized)
    {
        image.type='UIN8';
        image.format = ntohl(CVPixelBufferGetPixelFormatType(videoFrame));
        bytes_per_pixel=blc_image_get_bytes_per_pixel(&image);
        switch (bytes_per_pixel){
            case -1: EXIT_ON_ARRAY_ERROR(&image, "Variable pixel size (i.e. compression) not yet managed.");  //Variable pixel size i.e:JPEG
                break;
            case 1:break;
            default: image.add_dim(bytes_per_pixel);
        }
        image.add_dim( CVPixelBufferGetWidth(videoFrame));
        image.add_dim( CVPixelBufferGetHeight(videoFrame));
        initialized = 1;
    }
    
    CVPixelBufferLockBaseAddress(videoFrame, 0);
    image.data=CVPixelBufferGetBaseAddress(videoFrame);
    continue_run=callback(&image, user_data);
    CVPixelBufferUnlockBaseAddress(videoFrame, 0);
    if (!continue_run)  [NSApp terminate:NULL];


}
@end

START_EXTERN_C
void init_capture(int (*callback)(blc_array *image, void*), void*user_data)
{
	NSError *error; 
	QTCaptureSession					*capture_session;
	QTCaptureDevice						*device;
	QTCaptureDeviceInput				*device_input;
	QTCaptureDecompressedVideoOutput	*decompressed_video_output;
	
	capture *capture_instance = [[capture alloc] init];
	
	capture_instance->callback = callback;
	capture_instance->user_data = user_data;

	capture_session = [QTCaptureSession new];	
	device = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeVideo];
	if (![device open:&error]) EXIT_ON_ERROR("Opening device", [error localizedDescription]);			
	
	device_input = [[QTCaptureDeviceInput alloc] initWithDevice:device];
	if (![capture_session addInput:device_input error:&error]) EXIT_ON_ERROR("Setting Decompressed output.\n\t", [error localizedDescription]);
	
	decompressed_video_output = [QTCaptureDecompressedVideoOutput new];
	if(![capture_session addOutput:decompressed_video_output error:&error]) EXIT_ON_ERROR("Setting Decompressed output.\n\t", [error localizedDescription]);
	
	[decompressed_video_output setAutomaticallyDropsLateVideoFrames:YES];
	[decompressed_video_output setDelegate:capture_instance];

    [NSApplication sharedApplication];
    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular]; //doe not work on SDK 10.5
    [NSApp activateIgnoringOtherApps:YES];
    
    capture_instance->capture_session=capture_session;
    
//    printf("%s\n", [[decompressed_video_output pixelBufferAttributes] valueForKey:(id)kCVPixelBufferPixelFormatTypeKey]); try to print format
    
	[capture_session startRunning];
    [NSApp run];
	
}
END_EXTERN_C